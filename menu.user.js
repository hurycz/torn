// ==UserScript==
// @name         TORN buy menu
// @namespace    http://tampermonkey.net/
// @version      0.3.1
// @description  my torn
// @author       You
// @match        https://www.torn.com/imarket.php
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function addGlobalStyle(css) {
        var head, style;
        head = document.getElementsByTagName('head')[0];
        if (!head) { return; }
        style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = css;
        head.appendChild(style);
    }

    function initUserMenu() {
        $('#mainContainer').parent().append('<div class="my-user-menu"><div class="menu-title">My menu</div><ul class="my-user-menu-buy"></ul></div>');

        addGlobalStyle('.menu-title { font-weight: bold; }');
        addGlobalStyle('.my-user-menu { position: fixed; top: 200px; right: 10px; }');
        addGlobalStyle('.my-user-menu-buy li { cursor: pointer; margin: 5px 0px; }');
    }
    initUserMenu();


    function initBuyMenu() {
        $('.my-user-menu-buy').append('<li class="call-buy">BUY first</li>');
        $('.my-user-menu-buy .call-buy').on('click', function(){buyFirst();});

        $('.my-user-menu-buy').append('<li class="call-buy-all">BUY all</li>');
        $('.my-user-menu-buy .call-buy-all').on('click', function(){buyAll();});
    }
    initBuyMenu();

    function buyFirst() {
        console.log('buy first');
        buyItem($('.items ul.item').first());
    }

    var itemBougth = 0;
    function buyItem($item) {
        //console.log('buy item');
        $item.find('li.buy').first().click();
        var randomTime = Math.floor((Math.random() * 385) + 355);
        //console.log(randomTime);
        setTimeout(function(){
            $item.parent().find('.confirm-buy').find('a.yes-buy').click();
            itemBougth++;
            //console.log('bought '+itemBougth);
        }, randomTime);
    }

    var tiReload;
    var tiCounter = 0;
    function buyAll() {
        console.log('buy all');
        $('.items ul.item').each(function(index) {
            var randomTime = Math.floor((Math.random() * 600) + 1000);
            tiCounter += randomTime;
            //console.log(tiCounter);
            var that = $(this);
            setTimeout(function(){
                buyItem(that);
            }, tiCounter);
            
            clearTimeout(tiReload);
            tiReload = setTimeout(function(){location.reload();}, tiCounter + 2000);
        });
    }




})();