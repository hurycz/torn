// ==UserScript==
// @name         TORN company manager
// @namespace    http://tampermonkey.net/
// @version      0.1.0
// @description  my torn
// @author       You
// @match        https://www.torn.com/companies.php
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // SET MAX STORAGE IN COMPANY
    var storageMaxItems = 230000;
    
    
    
    
    // DO TOT CHANGE BELLOW
    var storageDayMultiplier = 0;
    $('li[aria-controls="pricing"]').on('click', function(){
        //console.log('click pricing');
        setTimeout(function(){
            var company = {};
            company['_COUNT'] = {};
            company['_COUNT']['dailySold'] = 0;

            $('.pricing-list-wrap .pricing-list').find('li').each(function(){
                var _name = $.trim($(this).find('.name').text());
                $(this).find('.sold-daily span').remove();
                var _dailySold = $.trim($(this).find('.sold-daily').text());
                $(this).find('.stock span').remove();
                var _stock = $.trim($(this).find('.stock').text());

                company[_name] = {};
                company[_name].dailySold = parseInt(_dailySold.replace(",", ""));
                company[_name].stock = parseInt(_stock.replace(",", ""));

                company['_COUNT']['dailySold'] += company[_name].dailySold;
            });

            //console.log(JSON.stringify(company));
            $.cookie('company-storage', JSON.stringify(company) );
        }, 200);
    });

    $('li[aria-controls="stock"]').on('click', function(){
        //console.log('click stock');
        setTimeout(function(){
            var _storage = JSON.parse($.cookie('company-storage'));

            // load from ordered items
            $('.order-list li').each(function(){
                if ($(this).find('.status').text() === "Delivered") {
                    $(this).hide();
                }
                else {
                    _storage[$.trim($(this).find('.name').text())]['stock'] += parseInt($.trim($(this).find('.amount').text().replace(",", "")));
                }
            });

//            console.log(_storage);
            storageDayMultiplier = Math.round((storageMaxItems / _storage['_COUNT']['dailySold']) * 100) / 100;

            var stockItems = 0;
            $('.stock-list-wrap .stock-list').find('li').not('.total').each(function(){
                var _name = $.trim($(this).find('.name').text());
                var _amount = _storage[_name].dailySold;

                _amount = (_amount * storageDayMultiplier) - _storage[_name].stock;
                if (_amount < 0) {
                    _amount = 0;
                }
                else if (_amount < 5000) {
                    _amount = Math.ceil(_amount / 500) * 500
                }
                else {
                    _amount = Math.ceil(_amount / 1000) * 1000
                }

                // todo fix keydown??
                $(this).find('input').val( Math.floor( _amount ));
            });

            console.log({
                dayMultiplier: storageDayMultiplier,
                dailySold: _storage['_COUNT']['dailySold']
            });

        }, 200);

    });
})();